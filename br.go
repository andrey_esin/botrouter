package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"

	"encoding/json"
)

func mainHandler(w http.ResponseWriter, req *http.Request) {

	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log.Println("Error: ", err)
		if len(body) <= 0 {
			w.WriteHeader(http.StatusNoContent)
			return
		}
		return
	}

	log.Println("BODY LEN: ", len(body))
	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte("OK\n"))
	log.Println("REQUEST: \n\t" + req.Host + "\n\tURI: " + req.RequestURI)

	cmd := exec.Command("./echobot.pl", "--json="+string(body))
	cmd.Stdout = os.Stdout
	err = cmd.Start()
	if err != nil {
		log.Println(err)
		return
	}
	log.Printf("PID: %d; Command: ./echobot.pl\n\n", cmd.Process.Pid)
}

type BotConfigStruct struct {
	runthis string
	webpath    string
	name    string
}

type ServersConfigStruct struct {
	bind	string
	port	int
	httpscert	string
	httpskey	string
}

type MainConfigurationStruct struct {
	Bots []BotConfigStruct
	Servers []ServersConfigStruct
	//Groups []string
}

func main() {

	file, _ := os.Open("config.json")
	decoder := json.NewDecoder(file)
	configuration := MainConfigurationStruct{}
	err := decoder.Decode(&configuration)
	if err != nil {
		fmt.Println("error:", err)
	}
//	fmt.Println(len(configuration.Bots))

	http.HandleFunc("/", mainHandler)
	go func() {
		err := http.ListenAndServeTLS(":8443", "server.crt", "server.key", nil)
		if err != nil {
			log.Fatal("HTTP Server Error: ", err)
		}
	}()

	log.Println("OK. Let's go!")

	select {}
}
